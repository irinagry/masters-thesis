set terminal png

pinf=108987
uinf=247.5

set xlabel "x"
set ylabel "cp"
set datafile separator ","

# Plot 0.01
set output "airfoilPlot_22062015_0.01_0.png"
plot "airfoil_0.01_00.csv" using ($5):((($1-pinf)/(-0.5*uinf*uinf))) with points title "OpenFOAM" ,\
	 "aerofoil_nasa.csv" using ($1):($2) with points title "NASA"

# Plot 0.02
set output "airfoilPlot_22062015_0.02_0.png"
plot "airfoil_0.02_00.csv" using ($5):((($1-pinf)/(-0.5*uinf*uinf))) with points title "OpenFOAM" ,\
	 "aerofoil_nasa.csv" using ($1):($2) with points title "NASA"

# Plot 0.03
set output "airfoilPlot_22062015_0.03_0.png"
plot "airfoil_0.03_00.csv" using ($5):((($1-pinf)/(-0.5*uinf*uinf))) with points title "OpenFOAM" ,\
	 "aerofoil_nasa.csv" using ($1):($2) with points title "NASA"

# Plot 0.04
set output "airfoilPlot_22062015_0.04_0.png"
plot "airfoil_0.04_00.csv" using ($5):((($1-pinf)/(-0.5*uinf*uinf))) with points title "OpenFOAM" ,\
	 "aerofoil_nasa.csv" using ($1):($2) with points title "NASA"
	 
# Plot 0.05
set output "airfoilPlot_22062015_0.05_0.png"
plot "airfoil_0.05_00.csv" using ($5):((($1-pinf)/(-0.5*uinf*uinf))) with points title "OpenFOAM" ,\
	 "aerofoil_nasa.csv" using ($1):($2) with points title "NASA"
	 
# Plot 0.06
set output "airfoilPlot_22062015_0.06_0.png"
plot "airfoil_0.06_00.csv" using ($5):((($1-pinf)/(-0.5*uinf*uinf))) with points title "OpenFOAM" ,\
	 "aerofoil_nasa.csv" using ($1):($2) with points title "NASA"

# Plot 0.07
set output "airfoilPlot_22062015_0.07_0.png"
plot "airfoil_0.07_00.csv" using ($5):((($1-pinf)/(-0.5*uinf*uinf))) with points title "OpenFOAM" ,\
	 "aerofoil_nasa.csv" using ($1):($2) with points title "NASA"

# Plot 0.08
set output "airfoilPlot_22062015_0.08_0.png"
plot "airfoil_0.08_00.csv" using ($5):((($1-pinf)/(-0.5*uinf*uinf))) with points title "OpenFOAM" ,\
	 "aerofoil_nasa.csv" using ($1):($2) with points title "NASA"

# Plot 0.09
set output "airfoilPlot_22062015_0.09_0.png"
plot "airfoil_0.09_00.csv" using ($5):((($1-pinf)/(-0.5*uinf*uinf))) with points title "OpenFOAM" ,\
	 "aerofoil_nasa.csv" using ($1):($2) with points title "NASA"
	 
# Plot 0.10
set output "airfoilPlot_22062015_0.10_0.png"
plot "airfoil_0.10_00.csv" using ($5):((($1-pinf)/(-0.5*uinf*uinf))) with points title "OpenFOAM" ,\
	 "aerofoil_nasa.csv" using ($1):($2) with points title "NASA"

# Plot 0.11
set output "airfoilPlot_22062015_0.11_0.png"
plot "airfoil_0.11_00.csv" using ($5):((($1-pinf)/(-0.5*uinf*uinf))) with points title "OpenFOAM" ,\
	 "aerofoil_nasa.csv" using ($1):($2) with points title "NASA"
	 
# Plot 0.12
set output "airfoilPlot_22062015_0.12_0.png"
plot "airfoil_0.12_00.csv" using ($5):((($1-pinf)/(-0.5*uinf*uinf))) with points title "OpenFOAM" ,\
	 "aerofoil_nasa.csv" using ($1):($2) with points title "NASA"

# Plot 0.13
set output "airfoilPlot_22062015_0.13_0.png"
plot "airfoil_0.13_00.csv" using ($5):((($1-pinf)/(-0.5*uinf*uinf))) with points title "OpenFOAM" ,\
	 "aerofoil_nasa.csv" using ($1):($2) with points title "NASA"

# Plot 0.14
set output "airfoilPlot_22062015_0.14_0.png"
plot "airfoil_0.14_00.csv" using ($5):((($1-pinf)/(-0.5*uinf*uinf))) with points title "OpenFOAM" ,\
	 "aerofoil_nasa.csv" using ($1):($2) with points title "NASA"
	 
# Plot 0.15
set output "airfoilPlot_22062015_0.15_0.png"
plot "airfoil_0.15_00.csv" using ($5):((($1-pinf)/(-0.5*uinf*uinf))) with points title "OpenFOAM" ,\
	 "aerofoil_nasa.csv" using ($1):($2) with points title "NASA"
	 
# Plot 0.16
set output "airfoilPlot_22062015_0.16_0.png"
plot "airfoil_0.16_00.csv" using ($5):((($1-pinf)/(-0.5*uinf*uinf))) with points title "OpenFOAM" ,\
	 "aerofoil_nasa.csv" using ($1):($2) with points title "NASA"
	 
# Plot 0.17
set output "airfoilPlot_22062015_0.17_0.png"
plot "airfoil_0.17_00.csv" using ($5):((($1-pinf)/(-0.5*uinf*uinf))) with points title "OpenFOAM" ,\
	 "aerofoil_nasa.csv" using ($1):($2) with points title "NASA"
	 
# Plot 0.18
set output "airfoilPlot_22062015_0.18_0.png"
plot "airfoil_0.18_00.csv" using ($5):((($1-pinf)/(-0.5*uinf*uinf))) with points title "OpenFOAM" ,\
	 "aerofoil_nasa.csv" using ($1):($2) with points title "NASA"
	 
# Plot 0.19
set output "airfoilPlot_22062015_0.19_0.png"
plot "airfoil_0.19_00.csv" using ($5):((($1-pinf)/(-0.5*uinf*uinf))) with points title "OpenFOAM" ,\
	 "aerofoil_nasa.csv" using ($1):($2) with points title "NASA"
	 
# Plot 0.20
set output "airfoilPlot_22062015_0.20_0.png"
plot "airfoil_0.20_00.csv" using ($5):((($1-pinf)/(-0.5*uinf*uinf))) with points title "OpenFOAM" ,\
	 "aerofoil_nasa.csv" using ($1):($2) with points title "NASA"
