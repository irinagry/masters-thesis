/*
 * Copyright (C) 2015 Irina Grigorescu <irinagry@gmail.com>
 */

// Points for the airfoil
cl1 = 0.005;
Point(1) = { 0.000000 ,0.000000, 0, cl1};
Point(2) = { 0.000602 ,0.003165, 0, cl1};
Point(3) = { 0.002408 ,0.006306, 0, cl1};
Point(4) = { 0.005412 ,0.009416, 0, cl1};
Point(5) = { 0.009607 ,0.012480, 0, cl1};
Point(6) = { 0.014984 ,0.015489, 0, cl1};
Point(7) = { 0.021530 ,0.018441, 0, cl1};
Point(8) = { 0.029228 ,0.021348, 0, cl1};
Point(9) = { 0.038060 ,0.024219, 0, cl1};
Point(10) = { 0.048005 ,0.027062, 0, cl1};
Point(11) = { 0.059039 ,0.029874, 0, cl1};
Point(12) = { 0.071136 ,0.032644, 0, cl1};
Point(13) = { 0.084265 ,0.035360, 0, cl1};
Point(14) = { 0.098396 ,0.038011, 0, cl1};
Point(15) = { 0.113495 ,0.040585, 0, cl1};
Point(16) = { 0.129524 ,0.043071, 0, cl1};
Point(17) = { 0.146447 ,0.045457, 0, cl1};
Point(18) = { 0.164221 ,0.047729, 0, cl1};
Point(19) = { 0.182803 ,0.049874, 0, cl1};
Point(20) = { 0.202150 ,0.051885, 0, cl1};
Point(21) = { 0.222215 ,0.053753, 0, cl1};
Point(22) = { 0.242949 ,0.055470, 0, cl1};
Point(23) = { 0.264302 ,0.057026, 0, cl1};
Point(24) = { 0.286222 ,0.058414, 0, cl1};
Point(25) = { 0.308658 ,0.059629, 0, cl1};
Point(26) = { 0.331555 ,0.060660, 0, cl1};
Point(27) = { 0.354858 ,0.061497, 0, cl1};
Point(28) = {0.378510 ,0.062133, 0, cl1};
Point(29) = {0.402455 ,0.062562, 0, cl1};
Point(30) = {0.426635 ,0.062779, 0, cl1};
Point(31) = {0.450991 ,0.062774, 0, cl1};
Point(32) = {0.475466 ,0.062530, 0, cl1};
Point(33) = {0.500000 ,0.062029, 0, cl1};
Point(34) = {0.524534 ,0.061254, 0, cl1};
Point(35) = {0.549009 ,0.060194, 0, cl1};
Point(36) = {0.573365 ,0.058845, 0, cl1};
Point(37) = {0.597545 ,0.057218, 0, cl1};
Point(38) = {0.621490 ,0.055344, 0, cl1};
Point(39) = {0.645142 ,0.053258, 0, cl1};
Point(40) = {0.668445 ,0.050993, 0, cl1};
Point(41) = {0.691342 ,0.048575, 0, cl1};
Point(42) = {0.713778 ,0.046029, 0, cl1};
Point(43) = {0.735698 ,0.043377, 0, cl1};
Point(44) = {0.757051 ,0.040641, 0, cl1};
Point(45) = {0.777785 ,0.037847, 0, cl1};
Point(46) = {0.797850 ,0.035017, 0, cl1};
Point(47) = {0.817197 ,0.032176, 0, cl1};
Point(48) = {0.835779 ,0.029347, 0, cl1};
Point(49) = {0.853553 ,0.026554, 0, cl1};
Point(50) = {0.870476 ,0.023817, 0, cl1};
Point(51) = {0.886505 ,0.021153, 0, cl1};
Point(52) = {0.901604 ,0.018580, 0, cl1};
Point(53) = {0.915735 ,0.016113, 0, cl1};
Point(54) = {0.928864 ,0.013769, 0, cl1};
Point(55) = {0.940961 ,0.011562, 0, cl1};
Point(56) = {0.951995 ,0.009508, 0, cl1};
Point(57) = {0.961940 ,0.007622, 0, cl1};
Point(58) = {0.970772 ,0.005915, 0, cl1};
Point(59) = {0.978470 ,0.004401, 0, cl1};
Point(60) = {0.985016 ,0.003092, 0, cl1};
Point(61) = {0.990393 ,0.002001, 0, cl1};
Point(62) = {0.994588 ,0.001137, 0, cl1};
Point(63) = {0.997592 ,0.000510, 0, cl1};
Point(64) = {0.999398 ,0.000128, 0, cl1};
Point(65) = {1.000000 ,0.000000, 0, cl1};

Point(128) = {0.000602 ,-.003160, 0, cl1};
Point(127) = {0.002408 ,-.006308, 0, cl1};
Point(126) = {0.005412 ,-.009443, 0, cl1};
Point(125) = {0.009607 ,-.012559, 0, cl1};
Point(124) = {0.014984 ,-.015649, 0, cl1};
Point(123) = {0.021530 ,-.018707, 0, cl1};
Point(122) = {0.029228 ,-.021722, 0, cl1};
Point(121) = {0.038060 ,-.024685, 0, cl1};
Point(120) = {0.048005 ,-.027586, 0, cl1};
Point(119) = {0.059039 ,-.030416, 0, cl1};
Point(118) = {0.071136 ,-.033170, 0, cl1};
Point(117) = {0.084265 ,-.035843, 0, cl1};
Point(116) = {0.098396 ,-.038431, 0, cl1};
Point(115) = {0.113495 ,-.040929, 0, cl1};
Point(114) = {0.129524 ,-.043326, 0, cl1};
Point(113) = {0.146447 ,-.045610, 0, cl1};
Point(112) = {0.164221 ,-.047773, 0, cl1};
Point(111) = {0.182803 ,-.049805, 0, cl1};
Point(110) = {0.202150 ,-.051694, 0, cl1};
Point(109) = {0.222215 ,-.053427, 0, cl1};
Point(108) = {0.242949 ,-.054994, 0, cl1};
Point(107) = {0.264302 ,-.056376, 0, cl1};
Point(106) = {0.286222 ,-.057547, 0, cl1};
Point(105) = {0.308658 ,-.058459, 0, cl1};
Point(104) = {0.331555 ,-.059046, 0, cl1};
Point(103) = {0.354858 ,-.059236, 0, cl1};
Point(102) = {0.378510 ,-.058974, 0, cl1};
Point(101) = {0.402455 ,-.058224, 0, cl1};
Point(100) = {0.426635 ,-.056979, 0, cl1};
Point(99) = {0.450991 ,-.055257, 0, cl1};
Point(98) = {0.475466 ,-.053099, 0, cl1};
Point(97) = {0.500000 ,-.050563, 0, cl1};
Point(96) = {0.524534 ,-.047719, 0, cl1};
Point(95) = {0.549009 ,-.044642, 0, cl1};
Point(94) = {0.573365 ,-.041397, 0, cl1};
Point(93) = {0.597545 ,-.038043, 0, cl1};
Point(92) = {0.621490 ,-.034631, 0, cl1};
Point(91) = {0.645142 ,-.031207, 0, cl1};
Point(90) = {0.668445 ,-.027814, 0, cl1};
Point(89) = {0.691342 ,-.024495, 0, cl1};
Point(88) = {0.713778 ,-.021289, 0, cl1};
Point(87) = {0.735698 ,-.018232, 0, cl1};
Point(86) = {0.757051 ,-.015357, 0, cl1};
Point(85) = {0.777785 ,-.012690, 0, cl1};
Point(84) = {0.797850 ,-.010244, 0, cl1};
Point(83) = {0.817197 ,-.008027, 0, cl1};
Point(82) = {0.835779 ,-.006048, 0, cl1};
Point(81) = {0.853553 ,-.004314, 0, cl1};
Point(80) = {0.870476 ,-.002829, 0, cl1};
Point(79) = {0.886505 ,-.001592, 0, cl1};
Point(78) = {0.901604 ,-.000600, 0, cl1};
Point(77) = {0.915735 ,0.000157, 0, cl1};
Point(76) = {0.928864 ,0.000694, 0, cl1};
Point(75) = {0.940961 ,0.001033, 0, cl1};
Point(74) = {0.951995 ,0.001197, 0, cl1};
Point(73) = {0.961940 ,0.001212, 0, cl1};
Point(72) = {0.970772 ,0.001112, 0, cl1};
Point(71) = {0.978470 ,0.000935, 0, cl1};
Point(70) = {0.985016 ,0.000719, 0, cl1};
Point(69) = {0.990393 ,0.000497, 0, cl1};
Point(68) = {0.994588 ,0.000296, 0, cl1};
Point(67) = {0.997592 ,0.000137, 0, cl1};
Point(66) = {0.999398 ,0.000035, 0, cl1};


// Lines for the airfoil
Spline(1000) = {1:33};
Spline(1001) = {33:65};
Spline(1002) = {65:97};
Spline(1003) = {97:128,1};

// Points for the outer box
edge_lc = 0.2;
Point(1100) = { -10,  10, 0, edge_lc};
Point(1101) = {0.5,  10, 0, edge_lc};
Point(1102) = {  10,  10, 0, edge_lc};
//Point(1103) = {  10,  0, 0, edge_lc};
Point(1104) = {  10, -10, 0, edge_lc};
Point(1105) = {0.5, -10, 0, edge_lc};
Point(1106) = { -10, -10, 0, edge_lc};

// Lines for the outer box
Line(1) = {1106, 1105};
Line(2) = {1105, 1104};
Line(3) = {1104, 1102};
//Line(4) = {1103, 1102};
Line(5) = {1102, 1101};
Line(6) = {1101, 1100};
Circle(7) = {1106, 1, 1100};

// Lines to help with the mesh
// Cone in front of airfoil
Line(10) = {1100, 1};  // top left with tip
Line(11) = {1106, 1};  // down left with tip
//Line(12) = {1103, 65}; // center right with tail

// Cone above airfoil
Line(13) = {1101, 33}; // center up with center up airfoil
// Cone below airfoil
Line(14) = {1105, 97}; // center down with center down airfoil
// Cone above right airfoil
Line(15) = {65, 1102}; // right up box with tip airfoil
// Cone below right airfoil
Line(16) = {1104, 65}; // right down box with tip airfoil



// Surface tip
Line Loop(20) = {-7, 11, -10};
Transfinite Line {-10} = 50 Using Progression 1.05;
Transfinite Line {-7}  = 50 Using Progression 1.05;
Transfinite Line {11}  = 50 Using Progression 1.05;
Plane Surface(50) = {20};
Transfinite Surface{50} = {1, 1100, 1106};
//Recombine Surface{50};

// Surface center up LEFT
Line Loop(21) = {13, -1000, -10, -6};
Transfinite Line {13}    = 50 Using Progression 1.05;
Transfinite Line {-1000} = 50 Using Progression 1.05;
Transfinite Line {-10}   = 50 Using Progression 1.05;
Transfinite Line {-6}    = 50 Using Progression 1.05;
Plane Surface(51) = {21};
Transfinite Surface{51} = {1101, 33, 1, 1100};
//Recombine Surface{51};

// Surface center down LEFT
Line Loop(22) = {1003, -11, 1, 14};
Transfinite Line {1003} = 50 Using Progression 1.05;
Transfinite Line {-11}  = 50 Using Progression 1.05;
Transfinite Line {1}    = 50 Using Progression 1.05;
Transfinite Line {14}   = 50 Using Progression 1.05;
Plane Surface(52) = {22};
Transfinite Surface{52} = {97, 1, 1106, 1105};
//Recombine Surface{52};

// Surface center up RIGHT
Line Loop(23) = {1001, 15, 5, 13};
Transfinite Line {1001} = 50 Using Progression 1.05;
Transfinite Line {15}   = 50 Using Progression 1.05;
Transfinite Line {5}    = 50 Using Progression 1.05;
Transfinite Line {13}   = 50 Using Progression 1.05;
Plane Surface(53) = {23};
Transfinite Surface{53} = {33, 65, 1102, 1101};
//Recombine Surface{53};

// Surface center down RIGHT
Line Loop(24) = {1002, -14, 2, 16};
Transfinite Line {1002} = 50 Using Progression 1.05;
Transfinite Line {-14}  = 50 Using Progression 1.05;
Transfinite Line {2}    = 50 Using Progression 1.05;
Transfinite Line {16}   = 50 Using Progression 1.05;
Plane Surface(54) = {24};
Transfinite Surface{54} = {97, 1105, 1104, 65};
//Recombine Surface{54};

// Surface tail
Line Loop(25) = {-15, -16, 3};
Transfinite Line {-15} = 50 Using Progression 1.05;
Transfinite Line {-16} = 50 Using Progression 1.05;
Transfinite Line {3}   = 50 Using Progression 1.05;
Plane Surface(55) = {25};
Transfinite Surface{55} = {65, 1104, 1102};
//Recombine Surface{55};



// We need to extrude the surface so that the mesh is 3D for OpenFOAM
Extrude {0, 0, 1} {
  Surface{50,51,52,53,54,55};
  Layers{1};
  Recombine;
}

//Volume(60) = {50,52,54,55,53,51};

Physical Surface("back") = {1042,1020,1064,1108,1125,1086}; 
Physical Surface("front") = {50,52,54,55,53,51,1019,1029,1077,1107,1063,1015}; 
Physical Surface("exit") = {1124}; 
Physical Surface("inlet") = {1081,1037,1011,1059,1103}; 
Physical Surface("aerofoil") = {1073,1029,1051,1095}; 
Physical Volume("internal") = {1,2,3,4,5,6};
